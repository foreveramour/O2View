//
//  PPLoadingCircleView.h
//  PPMoneyPro
//
//  Created by M on 2017/4/14.
//  Copyright © 2017年 PPMoney. All rights reserved.
//


#import <UIKit/UIKit.h>
@interface PPLoadingCircleView : UIView
@property (nonatomic, assign) CGFloat radius;
@end
