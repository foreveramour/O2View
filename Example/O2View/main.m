//
//  main.m
//  O2View
//
//  Created by 李夏 on 01/22/2018.
//  Copyright (c) 2018 李夏. All rights reserved.
//

@import UIKit;
#import "O2ViewAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([O2ViewAppDelegate class]));
    }
}
