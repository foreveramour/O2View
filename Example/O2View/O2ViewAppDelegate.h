//
//  O2ViewAppDelegate.h
//  O2View
//
//  Created by 李夏 on 01/22/2018.
//  Copyright (c) 2018 李夏. All rights reserved.
//

@import UIKit;

@interface O2ViewAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
